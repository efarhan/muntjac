from kivy.uix.image import Image

__author__ = 'efarhan'

import random
import kivy

random.seed(0)

type_nmb = 5

size = [20,20]

min_row_size = 3

cells = [ [random.randint(0,type_nmb-1) for i in range(size[1])] for j in range(size[0])]

path = "data/sprites/Chips/"

cells_image_map = ["chipBlueWhite","chipGreenWhite",
                   "chipRedWhite","chipBlackWhite","chipWhiteBlue"]

img_type = ["", "_border", "_side", "_sideBorder"]
textures = []

cursor = [-1, -1]

images = []

image_size = 100


class Tile(Image):
    def __init__(self,pos,**kwargs):
        self.tile_pos = pos
        super(Tile, self).__init__(**kwargs)
    def on_touch_down(self, touch):
        print "TOUCH",touch,self.tile_pos


def load_images(window):

    global cells_image_map,cells,image_size, images

    screen_size = [window.width,window.height]
    print "SCREEN", screen_size
    screen_smaller = min(screen_size[0],screen_size[1])
    image_size = screen_smaller/size[0]

    print image_size

    for img_name in cells_image_map:
        text_array = []
        for t in img_type:
            text_array.append(kivy.core.image.Image.load(filename=path+img_name+t+".png",
                                                       keep_data=True))
        textures.append(text_array)
    for i in range(size[0]):
        row = []
        for j in range(size[1]):
            img = Tile([i,j])
            img.texture = textures[cells[i][j]][0].texture

            img.x = i*image_size-screen_smaller/2+image_size/2
            img.y = j*image_size-screen_smaller/2+image_size/2
            '''img.size[0] = image_size
            img.size[1] = image_size
            img.texture_size[0] = image_size
            img.texture_size[1] = image_size'''
            img.width =img.height = image_size
            window.add_widget(img)
            row.append(img)
        images.append(row)


def generate_local_indicator(cells):

    local_ind = [[[0,0] for i in range(size[1])] for j in range(size[0])]
    for i in range(size[0]):
        for j in range(size[1]):
            if i + 1 != size[0] and cells[i+1][j] == cells[i][j]:
                local_ind[i][j][1] = 1
            if j + 1 != size[1] and cells[i][j+1] == cells[i][j]:
                local_ind[i][j][0] = 1
    return local_ind


def generate_global_indicator(local_ind):
    global_ind = [[[0,0] for i in range(size[1])] for j in range(size[0])]
    for i in range(size[0]):
        for j in range(size[1]):
            if i + 1 != size[0] and local_ind[i][j][1] == 1:
                global_ind[i][j][1] = 2
                for d_i in range(1,size[0]-i):
                    if i + d_i + 1 != size[0] and local_ind[i + d_i][j][1] == 1:
                        global_ind[i][j][1] += 1
                    else:
                        break
            if j + 1 != size[1] and local_ind[i][j][0] == 1:
                global_ind[i][j][0] = 2
                for d_j in range(1,size[1]-j):
                    if j + d_j + 1 != size[1] and local_ind[i][j+d_j][0] == 1:
                        global_ind[i][j][0] += 1
                    else:
                        break
    return global_ind


def mark_cell(global_ind):
    marker = [[0 for i in range(size[1])]for j in range(size[0])]
    for i in range(size[0]):
        for j in range(size[1]):
            if marker[i][j] == 0 and global_ind[i][j][0] >= min_row_size:
                marker[i][j] = global_ind[i][j][0]
                for d_j in range(1,min_row_size):
                    marker[i][j+d_j] = global_ind[i][j][0]
            if marker[i][j] == 0 and global_ind[i][j][1] >= min_row_size:
                marker[i][j] = global_ind[i][j][1]
                for d_i in range(1,min_row_size):
                    marker[i+d_i][j] = global_ind[i][j][1]
    return marker

def print_array(a):
    for i in range(len(a)):
        print a[i]
    print
print_array(cells)



'''
local_ind = generate_local_indicator(cells)
print_array(local_ind)
global_ind = generate_global_indicator(local_ind)
print_array(global_ind)
marker = mark_cell(global_ind)
print_array(marker)
'''
