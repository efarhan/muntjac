from kivy.clock import Clock
from grid import load_images

__author__ = 'efarhan'

import kivy

from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.widget import Widget


class MuntjacGame(Widget):
    def __init__(self,app,**kwargs):
        self.app = app
        super(MuntjacGame, self).__init__(**kwargs)
        load_images(self.app.window)

    def update(self, delta_time):
        pass


class MuntjacApp(App):

    def build(self):
        from kivy.base import EventLoop
        EventLoop.ensure_window()
        self.window = EventLoop.window

        self.muntjac_widget = MuntjacGame(app=self)
        self.root = self.muntjac_widget
        self.root.width = self.window.width
        self.root.height = self.window.height
        Clock.schedule_interval(self.muntjac_widget.update, 1.0 / 60.0)
        return self.root


if __name__ == '__main__':
    MuntjacApp().run()